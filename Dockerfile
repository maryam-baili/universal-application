ARG NODE_VERSION
ARG APP_ENV
FROM node:${NODE_VERSION} AS dev
WORKDIR /app
COPY package.json yarn.lock sonar-project.js ./

#RUN apk update
#RUN apk --update --no-cache add curl ca-certificates tar xz
RUN yarn install
RUN yarn add morgan --save
RUN yarn add multer --save
RUN yarn add mongoose --save
RUN yarn add body-parser --save


#RUN apk --no-cache add ca-certificates wget \
    #&& wget -q -O /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
    #&& wget https://github.com/sgerrand/alpine-pkg-glibc/releases/download/2.31-r0/glibc-2.31-r0.apk \
    #&& apk add glibc-2.31-r0.apk

#RUN curl -Lso /tmp/libz.tar.xz https://www.archlinux.org/packages/core/x86_64/zlib/download
#RUN mkdir -p /tmp/libz
#RUN tar -xf /tmp/libz.tar.xz -C /tmp/libz
#RUN cp /tmp/libz/usr/lib/libz.so.* /usr/glibc-compat/lib

#RUN yarn add --dev sonarqube-scanner
COPY src/ ./src/
EXPOSE 4000
CMD [ "yarn", "run", "start" ]
#RUN yarn sonar




FROM node:${NODE_VERSION} AS deps
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install --production


FROM node:${NODE_VERSION} AS prod
WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules/
COPY --from=dev /app ./
EXPOSE 4000
CMD ["node", "src/server.js"]
