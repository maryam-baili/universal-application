const express = require('express');
const router = express.Router();
const mongoose = require("mongoose");
const multer = require('multer');

const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
   fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

const Extracurricular = require("../models/extracurricular.model");

router.get("/get", (req, res, next) => {
  Extracurricular.find()
    .select("libelle publisher photo  file  video content level subject likes_count dislikes_count")
    .exec()
    .then(docs => {
      const response = {
        count: docs.length,
        extracurriculars: docs.map(doc => {
          return {
            libelle: doc.libelle,
            publisher: doc.publisher,
            photo: doc.photo,
            file: doc.file,
            video: doc.video,
            content: doc.content ,
            level: doc.level,
            subject: doc.subject, 
            likes_count: doc.likes_count, 
            dislikes_count: doc.dislikes_count, 
            _id: doc._id,
            request: {
              type: "GET",
              url: "http://localhost:3000/extracurriculars/get" + doc._id
            }
          };
        })
      };
      //   if (docs.length >= 0) {
      res.status(200).json(response);
      //   } else {
      //       res.status(404).json({
      //           message: 'No entries found'
      //       });
      //   }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.post("/post", upload.single('photo'), (req, res, next) => {
  const extracurricular = new Extracurricular({
    _id: new mongoose.Types.ObjectId(),
    libelle: req.body.libelle,
    publisher: req.body.publisher,
    photo: req.file.path,
    file: req.file.path,
    video: req.body.video,
    content: req.body.content ,
    level: req.body.level,
    subject: req.body.subject, 
    likes_count: req.body.likes_count, 
    dislikes_count: req.body.dislikes_count, 
  });
  extracurricular
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Created Extracurricular successfully",
        createdExtracurricular: {
            libelle: result.libelle,
            publisher: result.publisher,
            file: result.file,
            video: result.video,
            content: result.content ,
            level: result.level,
            subject: result.subject, 
            likes_count: result.likes_count, 
            dislikes_count: result.dislikes_count, 
            _id: result._id,
            request: {
                type: 'GET',
                url: "http://localhost:3000/extracurriculars/post" + result._id
            }
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

router.get("get/:extracurricularId", (req, res, next) => {
  const id = req.params.extracurricularId;
  Extracurricular.findById(id)
    .select('libelle publisher photo  file  video content level subject likes_count dislikes_count')
    .exec()
    .then(doc => {
      console.log("From database", doc);
      if (doc) {
        res.status(200).json({
          extracurricular: doc,
            request: {
                type: 'GET',
                url: 'http://localhost:3000/extracurriculars/get/:id'
            }
        });
      } else {
        res
          .status(404)
          .json({ message: "No valid entry found for provided ID" });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
});

router.patch("update/:extracurricularId", (req, res, next) => {
  const id = req.params.extracurricularId;
  //const updateOps = {};
  //for (const ops of req.body) {
  //  updateOps[ops.propName] = ops.value;
  //}
  //Extracurricular.update({ _id: id }, { $set: updateOps })
  Extracurricular.updateMany({_id: id}, {$set: req.body})
    .exec()
    .then(result => {
      res.status(200).json({
          message: 'Extracurricular updated',
          request: {
              type: 'GET',
              url: 'http://localhost:3000/extracurriculars/update' + id
          }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});


/*router.patch("/update/extracurriculars/like/:extracurricularId", (req, res, next) => {

  Extracurricular.findById(req.params.id)
  .exec()
    .then((like) => {
      like.count++;

      like.save()
        .then(() => res.json(like))
        .catch((err) => next(err));
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});*/


router.delete("delete/:extracurricularId", (req, res, next) => {
  const id = req.params.extracurricularId;
  Extracurricular.remove({ _id: id })
    .exec()
    .then(result => {
      res.status(200).json({
          message: 'Extracurricular deleted',
          request: {
              type: 'POST',
              url: 'http://localhost:3000/extracurriculars/delete',
              body: { libelle: 'String', publisher: 'String',  file: 'String',  video: 'String', content: 'String', level: 'Number', subject: 'String',likes_count: 'Number',dislikes_count: 'Number'}
          }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
});

module.exports = router;
