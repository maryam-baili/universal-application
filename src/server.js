
const express = require('express');
const mongodb = require('mongodb');
const bodyParser = require('body-parser');
const morgan = require("morgan");
const extracurricularRoutes = require("./app/controllers/extracurricular");
const config = require('./db');
//const PORT = 4000;
//const client = mongodb.MongoClient;
const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
 //Connecting to the databasemake
 mongoose.connect(dbConfig.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});


/*client.connect(config.DB, function(err, db) {
    if(err) {
        console.log('database is not connected')
    }
    else {
        console.log('connected!!')
    }
});*/


// create express app
const app = express();

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())
app.use(morgan("dev"));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  if (req.method === "OPTIONS") {
    res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
    return res.status(200).json({});
  }
  next();
});

// Routes which should handle requests
app.use("/extracurriculars", extracurricularRoutes);

/*app.use((req, res, next) => {
  const error = new Error("Not found");
  error.status = 404;
  next(error);
});

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
})*/;


app.get('/', function(req, res) {
  res.json({"message": "Welcome to Extacurriculars application. Take Extacurriculars quickly. Organize and keep track of all your Extacurriculars."});
});


// Require Comments routes
require('./app/routes/comment.routes.js')(app);
require('./app/routes/likeDislike.routes.js')(app);

// listen for requests
app.listen(4000, () => {
    console.log("Server is listening on port 3000");
});
