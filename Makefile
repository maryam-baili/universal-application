#!/usr/bin/env bash
include .env
export $(shell sed 's/=.*//' .env)


help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help


.PHONY: help commit build build-sonar run-sonar sonar-scanner run stop destroy 


PROJECT_NAME ?= private_school
CI_REGISTRY ?= registry.gitlab.com
CI_REGISTRY_IMAGESPACE ?= maryam-baili
CI_REGISTRY_IMAGE ?= $(CI_REGISTRY)/$(CI_REGISTRY_IMAGESPACE)/$(PROJECT_NAME)
CI_COMMIT_SHA ?= $(shell git rev-parse HEAD)

# DOCKER TASKS
build: ## build the project image.     
	@docker build --pull --cache-from $(CI_REGISTRY_IMAGE)/nodejs-release-image:latest -t $(CI_REGISTRY_IMAGE)/nodejs-test-image:$(CI_COMMIT_SHA) --rm --target $(APP_ENV) .\
        --build-arg NODE_VERSION=${NODE_VERSION} \
        

build-sonar: ## build sonarqube server image
		@docker build -t sonarqube -f sonar-server-dockerfile --rm .

run-sonar: ## run sonarqube image.
		@docker run -d --name=sonarqube -p 7000:9000 sonarqube


sonar-scanner: ## build sonar scanner image.
		@docker build -t sonarscanner -f sonar-dockerfile --network=host --rm .


run: ## Run an already built project
	@docker-compose up -d

stop: ## Stop an already running project
	@docker-compose stop

destroy: ## Destroy an already built project
	@docker-compose down
	@docker-compose rm -f





